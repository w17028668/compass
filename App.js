/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

 //Imports other components in order to be used in this one
import React, {Component} from 'react';
import StartingPage from './app/components/StartingPage';
import Menu from './app/components/Menu';
import FetchPlaces from './app/components/FetchPlaces';
import OnMap from './app/components/OnMap';
import UserProfile from './app/components/UserProfile';
import PlaceInfoPage from './app/components/PlaceInfoPage';
import Logout from './app/components/Logout';
import 'core-js/es6/symbol';
import 'core-js/fn/symbol/iterator';
import "core-js/es6/set";
import {
  createStackNavigator,
  createAppContainer,
} from 'react-navigation';

//component
class App extends Component {
  
  render() {

    return (
      <AppNavigator/>
    );

  }
}

//navigation for the app
const AppNavigator = createStackNavigator({

  StartingPage: StartingPage,
  Menu: Menu,
  FetchPlaces: FetchPlaces,
  PlaceInfoPage: PlaceInfoPage,
  OnMap: OnMap,
  UserProfile: UserProfile,
  Logout: Logout,

},{
  //navigation settings
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }

 }
);

//exports component
export default createAppContainer(AppNavigator);
