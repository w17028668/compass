import React, {Component} from 'react';
import {StyleSheet, View, AppRegistry, Image, TouchableOpacity, TextInput, Text, FlatList, Dimensions, ScrollView} from 'react-native';
import Navbar from './Navbar';
import {Rating} from 'react-native-elements';


export default class PlaceInfoPage extends Component{
    
    constructor(props) {
      super(props);
      this.state = {
        placeId: null,
        placeName: null, 
        photo: null,
        photoURL: null,
        loggedIn: null,
        token: null,
        reviewInput: null,
        rating: null,
        dataSource: null, //array with the json response
        item: {},
        openNow: false,
        geometry: {},
        newReview: false,
        pickerValue: null,
        placeName: null,
        lat: null,
        lng: null,
        total: null,
        reviewRating: null,
        rating: null,
        distance: null,
        address: null,
        price: null,
      };
    }

    //runs when component is ready
    componentDidMount = () => {

        const { navigation } = this.props;

        this.setState({
            placeId: navigation.getParam('id', 'NO-ID'),
            placeName: navigation.getParam('name', 'NO-NAME'),
            photo: navigation.getParam('photo', 'NO-PHOTO'),
            loggedIn: navigation.getParam('loggedIn', 'NO-LOGGEDIN'),
            token: navigation.getParam('token', 'NO-TOKEN'),
            openNow: navigation.getParam('open', 'NO-TOKEN'),
            lat: navigation.getParam('lat', 'NO-TOKEN'),
            lng: navigation.getParam('lng', 'NO-TOKEN'),
            rating: navigation.getParam('rating', 'NO-TOKEN'),
            total: navigation.getParam('total', 'NO-TOKEN'),
            distance: navigation.getParam('distance', 'NO-TOKEN'),
            address: navigation.getParam('address', 'NO-TOKEN'),
            venueName: navigation.getParam('venueName', 'NO-TOKEN'),
            description: navigation.getParam('description', 'NO-TOKEN'),
            startTime: navigation.getParam('startTime', 'NO-TOKEN'),
        });

        this.getReviews(navigation.getParam('id', 'NO-TOKEN'), navigation.getParam('token', 'NO-TOKEN') );
        this.getPhoto(navigation.getParam('photo', 'NO-PHOTO'));
    }

    //calls API for reviews
    getReviews = (placeId, token) =>{

      const URI = 'http://ec2-18-219-91-214.us-east-2.compute.amazonaws.com/compass-restful-api/public/api/auth/getReviews';//local

      return fetch(URI,
      { 
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token,
        },
        body: JSON.stringify({
          'place_id': placeId,
        })
          
      })
      .then((response) => response.json())
      .then((responseJson) => {
    
        this.setState({
          dataSource: responseJson.success,
        });

      })
      .catch((error) => {
          console.log(error);
      })
    }

    //publish reviews
    publishReview = () => {

      const URI = 'http://ec2-18-219-91-214.us-east-2.compute.amazonaws.com/compass-restful-api/public/api/auth/publish';//local
       
      return fetch(URI,
        { 
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.state.token,
          },
          body: JSON.stringify({
            'user_id': '',
            'place_id': this.state.placeId,
            'review_text': this.state.reviewInput,
            'rating': this.state.reviewRating,
            'name': this.state.placeName,
            'price': this.state.price,
          })
            
        })
        .then((response) => response.json())
        .then((responseJson) => {

          if(responseJson.success){

            this.setState({
              success: responseJson.success,
              newReview: false
            }, ()=>{

              setTimeout(()=>{
                this.setState({success: null})
              },10000)

            })
          }else{
      
            this.setState({
              errors: responseJson,
            }, ()=>{

              setTimeout(()=>{
                this.setState({errors: null})
              },10000)

            })
          }
        })
        .catch((error) => {
            console.log(error);
        })
    }

    //calls API for photo
    getPhoto = (photoReference) =>{

      const URI = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=1000&photoreference='+photoReference+'&key=AIzaSyAm7GbaRHzHLW-ef6sizCUNc6Ed4iN2Bpw';

      return fetch(URI)
      .then((responseJson) => {
       
        this.setState({
          photoURL: responseJson.url
        });

      })
      .catch((error) => {
          console.log(error);
      })
    }

    //sets rating
    setRating = (rating) => {
     
      this.setState({
        reviewRating: rating      
      });

    }

    //opens review form on click
    onClickOpen = () =>{

      if(this.state.newReview){

        this.setState({
          newReview: false
        })

      }else{

        this.setState({
          newReview: true
        })

      }
    }

    flatListItemSeparator = () => {
      return (
        <View style={styles.separator}/>
      );
    }

    displayErrors(state) {

      const errors = state;
    
      if(errors){
        const errorItems = Object.keys(errors.errors).map( (key, i) => {

          const error = errors.errors[key][0];

          return (
            <Text>{"\n"}{error}</Text>
          )
          
        });

        return (
            
            <Text>{errorItems}</Text>
        )
      }
    }

    render() {

      const win = Dimensions.get('window');
      const { navigation } = this.props;
      let ifErrors;
     
      if(this.state.errors){
       
        ifErrors = this.displayErrors(this.state.errors)
   
      }
 
      return (
       
          <View  style={styles.container}>
            <View style={styles.headerStyle}>
              <Navbar token = {this.state.token}></Navbar>
            </View>
            <View style={[{flex: 1}, styles.elementsContainer]}>
            {navigation.getParam('photo', 'NO-PHOTO') == 'NO-PHOTO'?
                <Image
                  resizeMode={'cover'}
                  style={{ 
                    alignSelf: 'stretch',
                    width: win.width,
                    height: 250,}}
                  source={require('../../assets/nophoto.png')}/>
                
              :  
               
                <Image
                  resizeMode={'cover'}
                  style={{ 
                    alignSelf: 'stretch',
                    width: win.width,
                    height: 250,}}
                  source={{uri: this.state.photoURL}}/>
                
              }
              <Text style={{fontSize: 26}}>{this.state.placeName}</Text>
              <ScrollView>
              <View style={{flex: 2}}>
                <View style={styles.descriptionContainer}>
                  <Text>Distance: {this.state.distance}km</Text>
                  {this.state.openNow === 'NO-TOKEN' ? null: <Text>Open now: {this.state.openNow === false? 'no' : 'yes' }</Text> }
                  {this.state.startTime === 'NO-TOKEN' ? null: <Text>Starting at: {this.state.startTime}</Text>}
                  <Text>Address: {this.state.address} {this.state.venueName === 'NO-TOKEN' ? null: ', ' + this.state.venueName}</Text>
                  <View style={styles.ratingContainer}>
                    <Text>{this.state.rating}</Text>
                    <Rating
                      type='custom'
                      ratingBackgroundColor='#c8c7c8'
                      imageSize={20}
                      readonly
                      startingValue={parseFloat(this.state.rating)}
                                
                     />
                    <Text>({this.state.total})</Text>
                  </View>
                  <View style={styles.buttonsContainer}>
                    <TouchableOpacity
                      style={styles.button}
                      onPress={() => this.props.navigation.navigate('OnMap', {
                                      lat: this.state.lat,
                                      lng: this.state.lng,
                                      placeName: this.state.placeName,
                                      rating: this.state.rating,
                                      token: this.state.token,
                                      address: this.state.address
                      })}
                    >
                      <Text style={styles.buttonText}>DIRECTIONS</Text>  
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.button}
                      onPress={() => this.onClickOpen()}
                    >
                      <Text style={styles.buttonText}>NEW REVIEW</Text>  
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.success}><Text>{this.state.success}</Text></View>
                {
                  this.state.newReview?
                  <View style={styles.reviewContainer}>
                    <View style={styles.errorsContainer}>{ifErrors}</View>
                    <View style={styles.reviewInnerContainer}>
                      <TextInput
                        style={styles.textbox}
                        onChangeText={(text) => this.setState({reviewInput: text})}
                        value={this.state.reviewInput}
                        placeholder="Write your review"
                      />
                
                      <View style={styles.inputsContainer}>
                        <TextInput
                          style={styles.input}
                          placeholder="Enter price"
                          onChangeText={(text) => this.setState({price: text})}
                          value={this.state.price}
                        />
                        <View style={styles.ratingContainer}>
                          <Rating
                            type='custom'
                            fractions='1' 
                            ratingBackgroundColor='#c8c7c8'
                            imageSize={25}
                            onFinishRating={this.setRating}
                            startingValue={0}
                            style={{marginBottom: 10}}
                          />
                          <Text>{this.state.reviewRating}</Text>
                        </View>
                      </View>
                      <TouchableOpacity
                        style={styles.buttonSubmit}
                        onPress={() => this.publishReview()}
                      >
                        <Text style={styles.buttonText}>PUBLISH</Text>  
                      </TouchableOpacity>
                    </View>
                    </View>
                  :
                  <FlatList
                  data={ this.state.dataSource }
                  ItemSeparatorComponent = {this.flatListItemSeparator}
                  renderItem={({item}) => {
                  return(

                    <View style={styles.reviewsContainer}>
                      <Text style={styles.flatListItemStyleTitle}> {item.user.name} </Text>
                      <Text style={styles.flatListItemStyle}> {item.review_text}</Text>
                      <View style={styles.reviewRatingContainer}>
                        <Rating
                          type='custom'
                          ratingBackgroundColor='#c8c7c8'
                          imageSize={20}
                          readonly
                          startingValue={item.rating}
                                  
                        />
                        <Text>{item.rating}</Text>
                      </View>
                      <Text style={styles.flatListItemStyle}> {item.created_at}</Text>
                    </View>
                  );
                }}
                keyExtractor={(item, index) => index.toString()}
                />
                
                }
              </View>
              </ScrollView>
            </View>
          </View>
  
      );
    }
  }
  
  const styles = StyleSheet.create({

    container: {
      flex: 1
    },
    headerStyle: {
      marginBottom: 50
    },
    pickerContainer:{
      flex: 1, 
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    },
    separator:{
      height: 1,
      width: "100%",
      backgroundColor: "#607D8B",
    },
    container: {
      flex: 1,
      justifyContent: 'center'
    },
    horizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      padding: 10
    },
    flatListItemStyle: {
      padding: 10,
      fontSize: 18,
      height: 44,
      color: '#111'
    },
    textbox: {
        height: 100,
        width: 220,
        marginBottom: 15, 
        borderColor: '#111',
        borderWidth: 1
    },
    input: {
      height: 40,
      width: 80,
      marginBottom: 15,
      marginRight: 10, 
      borderColor: '#111', 
      borderWidth: 1,
    },
    descriptionContainer: {
      width: '100%',
      height: 120
    },
    buttonsContainer:{
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignContent: 'center',
      marginTop: 20,
    },
    button: {
      width: 84,
      height: 32,
      backgroundColor: '#111',
      shadowOffset:{  width: 4,  height: 4,  },
      shadowColor: 'black',
      shadowOpacity: 0.25,
      justifyContent: 'center',
      alignContent: 'center',
    },
    buttonText:{
      textAlign:'center',
      fontSize: 14,
      fontFamily: 'FjallaOne-Regular',
      color: '#fff',
    },
    reviewContainer:{
      flex: 1,
      justifyContent: 'center',
      alignContent: 'center',
 
    },
    pickerContainer:{
      flex: 1, 
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    },
    ratingContainer:{
      flexDirection: 'row',
    },
    reviewInnerContainer:{
      justifyContent: 'center',
      alignItems: 'center'
    },
    flatListItemStyleTitle:{
      padding: 10,
      fontSize: 20,
      height: 44,
      fontWeight: 'bold',
      color: '#111'
    },
    reviewsContainer:{
      marginLeft: 10
    },
    reviewRatingContainer:{
      flexDirection: 'row',
      marginLeft: 10
    },
    errorsContainer:{
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: -10,
      marginBottom: 10,
    },
    success:{
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 10,
      marginBottom: 10,
    },
    inputsContainer:{
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    },
    buttonSubmit:{
      width: 84,
      height: 32,
      backgroundColor: '#111',
      shadowOffset:{  width: 4,  height: 4,  },
      shadowColor: 'black',
      shadowOpacity: 0.25,
      justifyContent: 'center',
      alignContent: 'center',
      marginBottom: 15
    }

  });

  AppRegistry.registerComponent('PlaceInfoPage', () => PlaceInfoPage);