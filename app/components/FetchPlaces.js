import React, {Component} from 'react';
import {StyleSheet, Text, View, FlatList, AppRegistry, TouchableOpacity, Image, Dimensions, Picker, ImageBackground} from 'react-native';
import Navbar from './Navbar';
import { Rating} from 'react-native-elements';
import {
  BarIndicator,
} from 'react-native-indicators';

export default class FetchPlaces extends Component{
    constructor(props) {
      super(props);
      this.state = {
        loggedIn: null,
        token: null,
        latitude: null, //latitude of the user
        longitude: null, //longitude of the user
        dataSource: null, //array with the json response
        error: null,
        categoryID: null, //categoryID for api call
        distance: null, //straight-line distance between two points(users location and place location)
        isLoading: true, // for loading while getting the json response
        photoURL: null,
        pickerValue: null,
        radius: null,
        preference: null

      };
    }
  
    //runs when component is ready
    componentDidMount = () => {

      const { navigation } = this.props;

      this.setState({
    
        loggedIn: navigation.getParam('loggedIn', 'NO-LOGGEDIN'),
        token: navigation.getParam('token', 'NO-TOKEN')
          
      });   
  
      navigator.geolocation.getCurrentPosition(position => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        });
        this.retrieveUserData(navigation.getParam('token', 'NO-TOKEN'), position.coords.latitude, position.coords.longitude); 

      });

     
    }
    
    //retrieves user data from API
    retrieveUserData = (token, latitude, longitude) => {

      const URI = 'http://ec2-18-219-91-214.us-east-2.compute.amazonaws.com/compass-restful-api/public/api/auth/details';//local
      var token = token;
    
      return fetch(URI,
      { 
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token,
        }
          
      })
      .then((response) => response.json())
      .then((responseJson) => {

        var radius = responseJson.success.details.radius;
        var preference = responseJson.success.details.preference;
        this.fetchPlaces(latitude, longitude, radius, preference);
         
      })
      .catch((error) => {
          console.log(error);
      })
    }
    
    apiCall = (url, preference) => {
      return fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
      
        //checks whats the categoryID 
        if(this.state.categoryID === 0 || this.state.categoryID === 1){

            
          //updates the state with the response
          this.getAverage(responseJson.results, 0, preference)

        }
        else if(this.state.categoryID === 2){
            
          this.getAverage(responseJson.events.event, 1, preference)

        }else{
            
          this.getAverage(responseJson.response.venues, 2, preference)

        }
      })
      .catch((error) => {

        this.setState({
      
          error: error //updates error state

        });

      });
    }
      
    //function to call for the API results
    fetchPlaces = (latitude, longitude, radius, preference) => {

      var latitude = latitude;
      var longitude = longitude;
      const { navigation } = this.props;
      var categoryID = navigation.getParam('categoryID', 'NO-CATEGORY');
      var apiCategory = navigation.getParam('apiCategory', 'NO-CATEGORY');
      var googleAPI = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+latitude+","+longitude+"+&radius="+radius+"&type=restaurant&key=AIzaSyAm7GbaRHzHLW-ef6sizCUNc6Ed4iN2Bpw";
      var googleAPIBars = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+latitude+","+longitude+"+&radius="+radius+"&type=bar&key=AIzaSyAm7GbaRHzHLW-ef6sizCUNc6Ed4iN2Bpw";
      var eventfulAPI = "http://api.eventful.com/json/events/search?app_key=jXdDBrc85ZsKNKB5&where="+latitude+","+longitude+"&within="+radius+"&units=m&page_size=20&date=This Week";
      var foursquareAPI = "https://api.foursquare.com/v2/venues/search?ll="+latitude+","+longitude+"&categoryId="+apiCategory+"&radius="+radius+"&client_id=HEBLBPSCAUGXX1HKPFSDWKYF3K03MB0DQ53HXKNJI1LMLDBE&client_secret=SQJAGVXTNKMJ53DVTZANWBPL1VQJSFMI0KRXTS035X3DXT2W&v=20180217";

      this.setState({

        categoryID: categoryID

      });
      
      if(this.state.categoryID === 0){

        this.apiCall(googleAPI, preference);

      }
      else if(this.state.categoryID === 1){

        this.apiCall(googleAPIBars, preference);

      }else if(this.state.categoryID === 2){

        this.apiCall(eventfulAPI, preference);

      }
      else{

        this.apiCall(foursquareAPI, preference);

      }
    }

    //calculates distance beetwen user and place
    getDistance = (placeLat, placeLon) => {

      var lat = this.state.latitude; //users latitude
      var lon = this.state.longitude; //users longitude
      var R = 6371; // Radius of the earth in km
      var dLat = (placeLat-lat) * (Math.PI/180);
      var dLon = (placeLon-lon) * (Math.PI/180); 

      var a = 
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(lat * (Math.PI/180)) * Math.cos(placeLat * (Math.PI/180)) * 
        Math.sin(dLon/2) * Math.sin(dLon/2);

      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
      var d = R * c; // Distance in km
      var d = Math.round(d * 100) / 100;
      
      return d;
    }
    
    //list separator
    flatListItemSeparator = () => {
      return (
        <View style={styles.separator}/>
      );
    }

    getAverage = (placesArray, id, preference) =>{

      const URI = 'http://ec2-18-219-91-214.us-east-2.compute.amazonaws.com/compass-restful-api/public/api/auth/getAverage';//local

      return fetch(URI,
        { 
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + this.state.token,
            },body: JSON.stringify({
              'placesArray': placesArray,
              'categoryID': id
            })
            
        }
        )
        .then((response) => response.json())
        .then((responseJson) => {
      
          if(id == 0){

            this.setState({
              dataSource: responseJson.success,
            });

            this.state.dataSource.forEach(function(entry){

              var distance = this.getDistance(entry.geometry.location.lat, entry.geometry.location.lng);
              entry.distance = distance; //new attribute here
            
            }.bind(this));

        
            this.filter(preference);

        }else if(id == 1){

          this.setState({
            dataSource: responseJson.success,
          });


          this.state.dataSource.forEach(function(entry){

            var distance = this.getDistance(entry.latitude, entry.longitude);
            entry.distance = distance; //new attribute here
          
          }.bind(this));
          console.log(preference);
          this.filter(preference);

          
        }else{
          console.log(preference);
          this.setState({
            dataSource: responseJson.success,
          });


          this.state.dataSource.forEach(function(entry){

            var distance = this.getDistance(entry.location.lat, entry.location.lng);
            entry.distance = distance; //new attribute here
          
          }.bind(this));
          console.log(preference);
          this.filter(preference);

        }
        
        this.setState({

          isLoading: false,

        })
        })
        .catch((error) => {
            console.log(error);
            
            
        })
      
    }

    //filtering system
    filter = (value) =>{

      function propComparator(prop) {

        if(prop === 'newRating'){

          return function(a, b) {
            return b[prop] - a[prop];
          }

        }
        else if(prop === 'cheapest'){

          return function(a, b) {
            return a['price'] - b['price'];
          }

        }
        else if(prop === 'priciest'){

          return function(a, b) {
            return b['price'] - a['price'];
          }

        }
        else{

          return function(a, b) {
            return a['distance'] - b['distance'];
          }

        }
      }
      this.setState({

        pickerValue: value,
        dataSource: this.state.dataSource.sort(propComparator(value)),
        refresh: !this.state.refresh

      })

    }

   
    render() {
      //loader
      const { rating } = this.props;

      const win = Dimensions.get('window');
      //loading screen
      if (this.state.isLoading) {

        return (
          <ImageBackground source={require('../../assets/bg.png')} style={{flex:1, height: undefined, width: undefined}} >
          <View style={styles.loadingContainer}>
         
            <Text style={{fontSize: 36, color: '#fff', fontFamily: 'FjallaOne-Regular', marginBottom: 150}}>LOADING</Text>
            <View style={styles.loading}>
              <BarIndicator color='white' count={4} size={80} />
            </View>
          </View>
          </ImageBackground>
        );
      }

        
      return (

        <View style={styles.container}>
          <ImageBackground source={require('../../assets/bg.png')} style={{flex:1, height: undefined, width: undefined}} >
            <View style={styles.headerStyle}>
              <Navbar token = {this.state.token}></Navbar>
            </View>
            <View style={[{flex: 1}, styles.elementsContainer]}>
              <View style={styles.titleContainer}>
              {this.state.categoryID === 2?
                <Text style={styles.titleText}>EVENTS NEAR YOU</Text>
                :
                <Text style={styles.titleText}>PLACES NEAR YOU</Text>
              }
              </View>
              <View style={styles.pickerContainer}>
                {this.state.categoryID === 5?
                  null
                  :
                <Text style={{marginRight: 20, fontSize: 20, color: '#fff'}}>Filter by:</Text>
                
                }
                 {this.state.categoryID === 5?
                  null
                  :this.state.categoryID === 0 || this.state.categoryID === 1 || this.state.categoryID === 3 || this.state.categoryID === 4?
                <View
                style={{
                  borderColor: '#111',
                  backgroundColor: '#E8E8E8',
                  borderWidth: 1,
                  width: 120,
                }}>
                
               
                <Picker
                  selectedValue={(this.state && this.state.pickerValue) || '1'}
                  style={{height: 35, width: 120}}
                  onValueChange={(itemValue, itemIndex) =>
                    this.filter(itemValue)
                  }>
                  <Picker.Item label="Closest" value="distance" />
                  <Picker.Item label="Highest Rating" value="newRating" />
                  <Picker.Item label="Cheapest" value="cheapest" />
                  <Picker.Item label="Priciest" value="priciest" />
                </Picker>
                </View>
                :
                <View
                style={{
                  borderColor: '#111',
                  backgroundColor: '#E8E8E8',
                  borderWidth: 1,
                  width: 120,
                }}>
                
               
                  <Picker
                  selectedValue={(this.state && this.state.pickerValue) || '1'}
                  style={{height: 35, width: 120}}
                  onValueChange={(itemValue, itemIndex) =>
                    this.filter(itemValue)
                  }>
                  <Picker.Item label="Closest" value="distance" />
                  <Picker.Item label="Highest Rating" value="newRating" />
                  </Picker>
                </View>
               }
              </View>
              <View style={{flex: 2}}>
                  <FlatList
                  
                  data={ this.state.dataSource }
                  extraData={this.state.refresh}
                  ItemSeparatorComponent = {this.flatListItemSeparator}
                  renderItem={({item}) => {

                  if(this.state.categoryID === 0 || this.state.categoryID === 1){

                      return(

                      <TouchableOpacity  onPress={() => this.props.navigation.navigate('PlaceInfoPage', {
                          id: item.place_id,
                          name: item.name,
                          photo: item.photos[0].photo_reference,
                          lat: item.geometry.location.lat,
                          lng: item.geometry.location.lng,
                          loggedIn: this.state.loggedIn,
                          token: this.state.token,
                          distance: item.distance,
                          rating: item.newRating,
                          total: item.reviewNum,
                          open: item.opening_hours.open_now,
                          address: item.vicinity
                      })}>
                          <Image
                          resizeMode={'cover'}
                          style={{ 
                          alignSelf: 'stretch',
                          width: win.width,
                          height: 250,}}
                          source={{uri: 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=1000&photoreference='+item.photos[0].photo_reference+'&key=AIzaSyAm7GbaRHzHLW-ef6sizCUNc6Ed4iN2Bpw'}}
                      />
                          <View style={styles.infoContainer}>
                            <Text style={styles.flatListItemStyle}> {item.name} </Text>
                            <Text style={styles.flatListItemStyle}> Distance: {item.distance} km </Text>
                            <Text style={styles.flatListItemStyle}> Avg Price: {item.price} £ </Text>
                            <View style={styles.ratingContainer}>
                              <Text>{item.newRating}</Text>
                              <Rating
                                type='custom'
                                ratingBackgroundColor='#c8c7c8'
                                imageSize={20}
                                readonly
                                startingValue={parseFloat(item.newRating)}
                                
                              />
                              <Text>({item.reviewNum})</Text>
                            </View>
                          </View>
                      </TouchableOpacity>

                      );

                  }else if(this.state.categoryID === 2){ 

                      return(

                      <TouchableOpacity  onPress={() => this.props.navigation.navigate('PlaceInfoPage', {
                          id: item.id,
                          name: item.title,
                          lat: item.latitude,
                          lng: item.longitude,
                          loggedIn: this.state.loggedIn,
                          token: this.state.token,
                          item: item,
                          rating: item.newRating,
                          total: item.reviewNum,
                          distance: item.distance,
                          description: item.description,
                          address: item.venue_address,
                          venueName: item.venue_name,
                          startTime: item.start_time
                      })}>
                  
                        <View style={styles.infoContainer}>
                          <Text style={styles.flatListItemStyle}> {item.title} </Text>
                          <Text style={styles.flatListItemStyle}> {(Math.round(item.distance)* 100)/1000} km </Text>
                          <View style={styles.ratingContainer}>
                              <Text>{item.newRating}</Text>
                              <Rating
                                type='custom'
                                ratingBackgroundColor='#c8c7c8'
                                imageSize={20}
                                readonly
                                startingValue={parseFloat(item.newRating)}
                                
                              />
                              <Text>({item.reviewNum})</Text>
                            </View>
                          </View>
                      </TouchableOpacity>

                      );

                  }else if(this.state.categoryID === 5){

                    return(

                      <TouchableOpacity  onPress={() => this.props.navigation.navigate('OnMap', {
                        id: item.id,
                        placeName: item.name,
                        lat: item.location.lat,
                        lng: item.location.lng,
                        loggedIn: this.state.loggedIn,
                        token: this.state.token,
                        distance: item.distance,
                        address: item.location.formattedAddress,
        
                      })}>
                        <View style={styles.infoContainer}>
                          <Text style={styles.flatListItemStyle}> {item.name} </Text>
                          <Text style={styles.flatListItemStyle}> {item.distance} km </Text>
                        </View>
                      </TouchableOpacity>
                      
                      );

                  }
                  else{

                      return(

                      <TouchableOpacity  onPress={() => this.props.navigation.navigate('PlaceInfoPage', {
                        id: item.id,
                        name: item.name,
                        lat: item.location.lat,
                        lng: item.location.lng,
                        loggedIn: this.state.loggedIn,
                        token: this.state.token,
                        item: item,
                        rating: item.newRating,
                        total: item.reviewNum,
                        distance: item.distance,
                        address: item.location.formattedAddress,
                      })}>
                        <View style={styles.infoContainer}>
                          <Text style={styles.flatListItemStyle}> {item.name} </Text>
                          <Text style={styles.flatListItemStyle}> {item.distance} km </Text>
                          <Text style={styles.flatListItemStyle}> Avg Price: {item.price} £ </Text>
                          <View style={styles.ratingContainer}>
                            <Text>{item.newRating}</Text>
                            <Rating
                              type='custom'
                              ratingBackgroundColor='#c8c7c8'
                              imageSize={20}
                              readonly
                              startingValue={parseFloat(item.newRating)}
                                
                            />
                            <Text>({item.reviewNum})</Text>
                          </View>
                        </View>
                      </TouchableOpacity>
                      
                      );
                  }
                  }}
                  keyExtractor={(item, index) => index.toString()}
                  />
                  </View>
                </View>
            </ImageBackground>
        </View>
    
        
  
      );
    }
  }
  
  const styles = {
    container: {
      flex: 1
    },
    headerStyle: {
      marginBottom: 50
    },
    titleContainer:{
      width: '100%', 
      height: 85,
      backgroundColor: '#fff',
      justifyContent: 'center',
      alignContent: 'center',
    },
    titleText:{
      textAlign:'center',
      fontSize: 26,
      color: '#111',
      fontFamily: 'FjallaOne-Regular',
    },  
    pickerContainer:{
      height: 150,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    },
    infoContainer: {
      backgroundColor: '#fff',
      justifyContent: 'center',
      alignItems: 'center',
      padding: 20
    },
    separator:{
      height: 1,
      width: "100%",
      backgroundColor: "#607D8B",
    },
    container: {
      flex: 1,
      justifyContent: 'center'
    },
    horizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      padding: 10
    },
    flatListItemStyle: {
      padding: 10,
      fontSize: 18,
      height: 44,
    },
    activityIndicator:{
      height: 80
    },
    ratingContainer:{
      flex: 1,
      flexDirection: 'row',
 
    },
    loadingContainer:{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    loading:{
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
    
    }
  }
  AppRegistry.registerComponent('FetchPlaces', () => FetchPlaces);