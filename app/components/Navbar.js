import React, {Component} from 'react';
import {StyleSheet, View, TouchableOpacity, FlatList, Text, Dimensions} from 'react-native';
import { Icon } from 'react-native-elements';
import { withNavigation } from 'react-navigation';



class Navbar extends Component{
    
    constructor(props) {
      super(props);
      this.state = {
        //here if needed
        loggedIn: null,
        inputLinkClicked: false
      };
    }

    //opens menu on click
    openMenu = () => {
      
      if(this.state.inputLinkClicked){

        this.setState({
          inputLinkClicked: false
        })

      }else{

        this.setState({
          inputLinkClicked: true
        })

      }
    }

    //back button
    backStock(){

      this.props.navigation.goBack()
      
      this.setState({
        inputLinkClicked: true
      })
   
    }
  
    
    render() {
   
      return (
        <View style={styles.mainContainer}>
        <View style={styles.navbar}>
          {this.props.menu == true?
            null
            :
          <Icon name='navigate-before' color='#fff' style={styles.navbarIcon} onPress={() => this.backStock()}/>
          }
          <View style={styles.menuIcon} >
            <Icon name='menu' color='#fff' onPress={() => this.openMenu()}/>
          </View>
        </View>
        <View>
        {
          this.state.inputLinkClicked?

          <View style={styles.menu}>
              <View style={styles.closeContainer}>
                <Icon name='close' color='#fff' style={styles.navbarIcon} onPress={() => this.openMenu()}/>
              </View>
              <Text style={styles.title}>COMPASS</Text>
              <FlatList
                data={[
                  {key: 'Profile', value: 'UserProfile'},
                  {key: 'Pick A place', value: 'Menu'},
                  {key: 'Logout', value: 'Logout'},
                ]}
                renderItem={({item}) =>  <TouchableOpacity onPress={() =>  this.props.navigation.navigate(item.value,  {
                    token: this.props.token
                })}><Text style={styles.items}>{item.key}</Text></TouchableOpacity>}
              />
          </View>
          :

          <View></View>
        }
        </View>
        </View>
      );
    }
  }
  
  const styles = StyleSheet.create({
    menu:{
      width: 200,
      height:  Dimensions.get('window').height,
      backgroundColor: '#1A1A1A',
      zIndex: 1,
      position: 'absolute'
    },
    image : {
      width: 110, 
      height: 105,
      marginBottom: 100
    },
    navbar:{
      position: 'absolute',
      width: "100%",
      height: 50,
      backgroundColor: '#100A2A',
      flexDirection: 'row',
      alignItems: 'center',
      zIndex: 0,
    },
    menuIcon:{
      position: 'absolute', 
      right: 10
     
    },
    title:{
      color: '#fff',
      textAlign: 'center',
      fontSize: 32,
      marginBottom: 65,
      marginTop: 40,
      fontFamily: 'FjallaOne-Regular',
    },
    items:{
      color: '#fff',
      textAlign: 'center',
      marginBottom: 20,
      fontSize: 20,
      fontFamily: 'FjallaOne-Regular',
    },
    closeContainer:{
      flexDirection: 'row-reverse',
      alignItems: 'center',
    }

  });

  export default withNavigation(Navbar);