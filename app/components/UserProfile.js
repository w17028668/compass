//imports other components
import React, {Component} from 'react'; 
import {StyleSheet, View, AppRegistry, TouchableOpacity, FlatList, Text, TextInput, ImageBackground, Picker} from 'react-native';
import {Rating} from 'react-native-elements';
import Navbar from './Navbar';

export default class UserProfile extends Component{
    
    //states
    constructor(props) {
      super(props);
      this.state = {
        uidInput: null,
        pwdInput: null,
        loggedIn: false,
        token: null,
        id: null,
        userName: null,
        userEmail: null,
        inputLinkClicked: false,
        previousPage: null,
        reviews: [],
        placeName: [],
        changePassword: false,
        pwdRegisterInput: null,
        pwdConfInput: null,
        pwdSuccess: false,
        errors: null,
        setPreferences: false,
        radius: null,
        pickerValue: null,
        userRadius: null,
        preference: null,
      };
    }

    //runs when component builds
    componentDidMount = () => {

      const { navigation } = this.props;

      this.setState({
        //gets values from previous component 
        loggedIn: navigation.getParam('loggedIn', 'NO-loggedIn'),
        token: navigation.getParam('token', 'NO-token'),
        previousPage: navigation.getParam('previousPage', 'NO-previousPage')

      });
      //calls the retrieveUserData function to displays the users details on load  
      this.retrieveUserData(navigation.getParam('token', 'NO-token'));

    }

    //calls restful API for details
    retrieveUserData = (token) => {

      const URI = 'http://ec2-18-219-91-214.us-east-2.compute.amazonaws.com/compass-restful-api/public/api/auth/details';
      var token = token;

      return fetch(URI,
      { 

        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token,
        }
            
      }
      )
      .then((response) => response.json())
      .then((responseJson) => {

        //sets states with the response from the the other half of the application
        this.setState({
          id: responseJson.success.details.id,
          userName: responseJson.success.details.name,
          userEmail: responseJson.success.details.email,
          preference: responseJson.success.details.preference,
          userRadius: responseJson.success.details.radius,
          reviews: responseJson.success.reviews,
        });
      })
      .catch((error) => {
        console.log(error);
      })
    }

    //calls restful API to delete the account
    deleteAccount = () =>{
  
      const URI = 'http://ec2-18-219-91-214.us-east-2.compute.amazonaws.com/compass-restful-api/public/api/auth/delete';
     
      return fetch(URI,
      { 
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.state.token,
        }
            
      }
      )
      .then((response) => response.json())
      .then((responseJson) => {

        //if the account is deletes redirect to starting screen
        if(responseJson.success){
          this.props.navigation.navigate('StartingPage')
        }
          
      })
      .catch((error) => {
        console.log(error);
      })

    }

    //conditional rendering on click
    displayForm = (state) =>{

      if(state == 'preferences'){

        if(this.state.setPreferences){

          this.setState({
            setPreferences: false
          })
  
        }else{
          
          this.setState({
            setPreferences: true
          })
  
        }
      }else{

        if(this.state.changePassword){

          this.setState({
            changePassword: false
          })
  
        }else{

          this.setState({
            changePassword: true
          })
  
        }
      }
    }

    //calls restfull API to reset pwd
    resetPassword = () =>{
    
      const URI = 'http://ec2-18-219-91-214.us-east-2.compute.amazonaws.com/compass-restful-api/public/api/auth/update';//local
     
      return fetch(URI,
      { 
        method: 'POST',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.state.token,
        },body: JSON.stringify({
          'password': this.state.pwdRegisterInput, //params for back-end
          'c_password': this.state.pwdConfInput,
        })
            
      })
      .then((response) => response.json())
      .then((responseJson) => {

        if(responseJson.success){

          this.setState({
            pwdSuccess: responseJson.success,
            changePassword: false
          }, ()=>{
              //message disappears in 10 seconds
              setTimeout(()=>{
                this.setState({pwdSuccess: null})
              },10000)

            })
          }else{
            
            this.setState({
              errors: responseJson,
            }, ()=>{

              setTimeout(()=>{
                this.setState({errors: null})
              },10000)

            })
          }
           
        })
        .catch((error) => {
            console.log(error);
        })
    }

    //calls restful API to set preferences
    setPreferences = () =>{
  
      const URI = 'http://ec2-18-219-91-214.us-east-2.compute.amazonaws.com/compass-restful-api/public/api/auth/setPreferences';//local
     
      return fetch(URI,
      { 
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + this.state.token,
        },body: JSON.stringify({
          'radius': this.state.radius,
          'preference': this.state.pickerValue,
        })
            
      })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.success){
          this.setState({
            pwdSuccess: responseJson.success,
            setPreferences: false
          }, ()=>{
            setTimeout(()=>{
              this.setState({pwdSuccess: null})
            },10000)
          })
        }else{
   
          this.setState({
            errors: responseJson,
          }, ()=>{

            setTimeout(()=>{
              this.setState({errors: null})
            },10000)
            
            })
          }
        
        })
        .catch((error) => {
            console.log(error);
        })
    }
    
    //displays errors
    displayErrors(state) {

      const errors = state;
    
      if(errors){

        const errorItems = Object.keys(errors.errors).map( (key, i) => {

          const error = errors.errors[key][0];

          return (
            <Text>{"\n"}{error}</Text>
          )
          
        });

        return (
            
            <Text>{errorItems}</Text>
        )
      }
    }
    
    //list separator
    flatListItemSeparator = () => {
      return (
        <View style={styles.separator}/>
      );
    }

    render() {

      let ifErrors;
     
      if(this.state.errors){
       
        ifErrors = this.displayErrors(this.state.errors)
   
      }
    
      return (
    
        <View  style={styles.container}>
          <ImageBackground source={require('../../assets/bg.png')} style={{flex:1, height: undefined, width: undefined}} >
            <View style={styles.headerStyle}>
              <Navbar token = {this.state.token}></Navbar>
            </View>
            <View style={[{flex: 1, backgroundColor: '#fff'}]}>
              <View style={{backgroundColor: '#fff'}}>
                <View style={styles.elementsContainer}>
                  <Text>username: {this.state.userName}</Text>
                  <Text>email:{this.state.userEmail}</Text>
                  <Text>radius:{this.state.userRadius}</Text>
                  {
                    this.state.preference === "distance"?
                    
                      <Text>preference: closest</Text>

                    :this.state.preference === "newRating"?

                      <Text>preference: highest rating</Text>

                    : 
                      <Text>preference: none</Text>
                  }
                </View>
                
                <View style={styles.buttonsContainer}>
                 <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.displayForm('preferences')}
                  >
                    <Text style={styles.buttonText}>Preferences</Text>  
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.deleteAccount()}
                  >
                    <Text style={styles.buttonText}>Delete Account</Text>  
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.displayForm('pwd')}
                  >
                    <Text style={styles.buttonText}>Change password</Text>  
                  </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.success}><Text>{this.state.pwdSuccess}</Text></View>
                {
                   this.state.changePassword?
                   
                   <View style={styles.inputsContainer}>
                     <View style={styles.errorsContainer}>{ifErrors}</View>
                     <TextInput
                        secureTextEntry={true}
                        placeholder="Enter password"
                        placeholderTextColor= "#fff" 
                        style={styles.input}
                        onChangeText={(text) => this.setState({pwdRegisterInput: text})}
                        value={this.state.pwdRegisterInput}
                      />
                      <TextInput
                        secureTextEntry={true}
                        placeholder="Enter password confirmation"
                        placeholderTextColor= "#fff"  
                        style={styles.input}
                        onChangeText={(text) => this.setState({pwdConfInput: text})}
                        value={this.state.pwdConfInput}
                      />
            
                    <TouchableOpacity
                      style={styles.button}
                      onPress={() => this.resetPassword()}
                    >
                      <Text style={styles.buttonText}>Change password</Text>  
                    </TouchableOpacity>
             
                   </View>
          
                 :this.state.setPreferences?
                  <View style={styles.inputsContainer}>
                    <View style={styles.errorsContainer}>{ifErrors}</View>
                    <TextInput
                        placeholder="Enter radius in meters"
                        placeholderTextColor= "#fff" 
                        style={styles.input}
                        onChangeText={(text) => this.setState({radius: text})}
                        value={this.state.radius}
                      />
              
                    <Picker
                      selectedValue={(this.state && this.state.pickerValue) || '1'}
                      style={{height: 35, width: 120,  marginBottom: 15}}
                      onValueChange={(itemValue, itemIndex) =>
                        this.setState({
                          pickerValue: itemValue,
                  
                        })
                      }>
                      <Picker.Item label="Closest" value="distance" />
                      <Picker.Item label="Highest Rating" value="newRating" />
                      <Picker.Item label="Cheapest" value="cheapest" />
                      <Picker.Item label="Priciest" value="priciest" />
                    </Picker>
            
                    <TouchableOpacity
                      style={styles.button}
                      onPress={() => this.setPreferences()}
                    >
                      <Text style={styles.buttonText}>Set</Text>  
                    </TouchableOpacity>
            
                </View>

                 :
     
                 <View></View>

                }
              <View style={{flex: 2}}>
                <View>
                  <View style={styles.reviewsText}>
                    <Text style={{color: '#fff', fontSize: 22, fontFamily: 'FjallaOne-Regular'}}>MY REVIEWS</Text>
                  </View>
                </View>
                <FlatList
                  data={ this.state.reviews }
                  ItemSeparatorComponent = {this.flatListItemSeparator}
                  renderItem={({item}) => {
                  return(
                    <View style={styles.reviewsContainer}>
                      <Text style={styles.flatListItemStyleTitle}> {item.name}</Text>
                      <Text style={styles.flatListItemStyle}> {item.review_text}</Text>
                      <View style={styles.reviewRatingContainer}>
                        <Rating
                          type='custom'
                          ratingBackgroundColor='#c8c7c8'
                          imageSize={20}
                          readonly
                          startingValue={item.rating}
                                  
                        />
                        <Text>{item.rating}</Text>
                      </View>
                      <Text style={styles.flatListItemStyle}> {item.created_at}</Text>
                    </View>
                  );
                }}
                //keyExtractor={(item, index) => index.toString()}
                />
              </View>
            </View>
            </ImageBackground>
          </View>

      );
    }
  }
  
  //styles
  const styles = StyleSheet.create({

    container: {
      flex: 1
    },
    headerStyle: {
      marginBottom: 50
    },
    pickerContainer:{
      flex: 1, 
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    },
    separator:{
      height: 1,
      width: "100%",
      backgroundColor: "#607D8B",
    },
    container: {
      flex: 1,
      justifyContent: 'center'
    },
    elementsContainer:{
      justifyContent: 'center',
      alignItems: 'center'
    },
    horizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      padding: 10
    },
    flatListItemStyle: {
      padding: 10,
      fontSize: 18,
      height: 44,
    },
    input: {
      height: 100,
      width: 200,
      marginBottom: 15, 
      borderColor: 'gray', 
      borderWidth: 1
    },
    descriptionContainer: {
      width: '100%',
      height: 150
    },
    buttonsContainer:{
      flexDirection: 'row',
      justifyContent: 'space-around',
      alignContent: 'center',
      marginTop: 20,
    },
    button: {
      width: 90,
      height: 46,
      backgroundColor: '#111',
      shadowOffset:{  width: 4,  height: 4,  },
      shadowColor: 'black',
      shadowOpacity: 0.25,
      justifyContent: 'center',
      alignContent: 'center',
    },
    buttonText:{
      textAlign:'center',
      fontSize: 14,
      fontFamily: 'FjallaOne-Regular',
      color: '#fff',
    },
    reviewContainer:{
      justifyContent: 'center',
      alignItems: 'center'
    },
    pickerContainer:{
      flex: 1, 
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    },
    inputsContainer:{
      width: '100%',
      marginTop: 20,
      marginBottom: 20,
      justifyContent: 'center',
      alignItems: 'center'
    },
    input: {
      height: 40,
      width: 200,
      backgroundColor: '#3591A1',
      marginBottom: 15, 
      borderColor: '#111', 
      borderWidth: 1,
    },
    reviewsText:{
      width: '100%',
      height: 85,
      backgroundColor: '#111',
      justifyContent: 'center',
      alignItems: 'center'
    },
    flatListItemStyleTitle:{
      padding: 10,
      fontSize: 20,
      height: 44,
      fontWeight: 'bold',
      color: '#111'
    },
    reviewsContainer:{
      marginLeft: 10
    },
    reviewRatingContainer:{
      flexDirection: 'row',
      marginLeft: 10
    },
    flatListItemStyle: {
      padding: 10,
      fontSize: 18,
      height: 44,
      color: '#111'
    },
    errorsContainer:{
      marginBottom: 15,
    },
    success:{
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 20,
      marginBottom: 20,
    }
    
  });

  AppRegistry.registerComponent('UserProfile', () => UserProfile);