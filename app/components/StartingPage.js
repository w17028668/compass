import React, {Component} from 'react';
import {StyleSheet, View, AppRegistry, Image, TouchableOpacity, TextInput, Text, ImageBackground, ScrollView, Alert } from 'react-native';

export default class StartingPage extends Component{
    
    constructor(props) {
      super(props);
      this.state = {
        loginClicked: false,
        registerClicked: false,
        nameInput: null,
        emailRegister: null,
        pwdRegisterInput: null,
        pwdConfInput: null,
        uidInput: null,
        pwdInput: null,
        token: null,
        loggedIn: false,
        errors: null,
        regErrors: null,
        registered: false,
        latitude: null,
        longitude: null,
      };
    }

    componentDidMount = () => {
      
      //gets users position
      navigator.geolocation.getCurrentPosition(position => {

        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        }); 

      },
      error => Alert.alert('Make sure your gps is on and restart the app.'),

      { enableHighAccuracy: false, timeout: 10000});

    }

    //registers user
    onClickRegister = () => {

      const URI = 'http://ec2-18-219-91-214.us-east-2.compute.amazonaws.com/compass-restful-api/public/api/register'; 

      return fetch(URI,
      { 
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({name: this.state.nameInput, email: this.state.emailRegister, password: this.state.pwdRegisterInput, c_password: this.state.pwdConfInput})}
      )
      .then((response) => response.json())
      .then((responseJson) => {
         
        if(responseJson.success){
        
          this.setState({
            registered: true
          });
          
        }else{
        
          this.setState({
            regErrors: responseJson,
          }, ()=>{

            setTimeout(()=>{
              this.setState({regErrors: null})
            },10000)

            })
          }
      })
      .catch((error) => {
          console.log(error);
      })
    }

    //logs in 
    onClickLogin = () => {

      const URI = 'http://ec2-18-219-91-214.us-east-2.compute.amazonaws.com/compass-restful-api/public/api/auth/login'; 
    
      return fetch(URI,
      { 
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({email: this.state.uidInput, password: this.state.pwdInput})}
      )
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.success){
              
          this.setState({
            loggedIn: true,
            token: responseJson.success.token
          });
          
          //if sucess redirects to Menu
          this.props.navigation.navigate('Menu', {
            loggedIn: this.state.loggedIn,
            token: this.state.token,
            latitude: this.state.latitude,
            longitude: this.state.longitude,
          })
          }else{
          
            this.setState({
              errors: responseJson,
            }, ()=>{

              setTimeout(()=>{
                this.setState({errors: null})
              },20000)

            })
          }    
        })
        .catch((error) => {
            console.log(error);
        })
    }

    //displays errors
    displayErrors(state) {

      const errors = state;
    
      if(errors){
        const errorItems = Object.keys(errors.errors).map( (key, i) => {

          const error = errors.errors[key][0];

          return (
            <Text>{"\n"}{error}</Text>
          )
          
        });

        return (
            <Text>{errorItems}</Text>
        )
      }
    }

    //checks if users gps is on
    checkIfLoc(){
      if(this.state.latitude){
        this.setState({ loginClicked: true })
      }else{
        Alert.alert('Make sure your gps is on and restart the app.')
      }
    }

    render() {

      let ifErrors;
     
      if(this.state.errors || this.state.regErrors){

        if(this.state.loginClicked){

          ifErrors = this.displayErrors(this.state.errors)

        }
        else{

          ifErrors = this.displayErrors(this.state.regErrors)

        }  
      }
    
      return (

        <ImageBackground    source={require('../../assets/bg.png')} style={{flex:1, height: undefined, width: undefined}} >
        <ScrollView>
        <View style={styles.mainContainer}>
            <Image
              style={styles.logo}
              source={require('../../assets/logo.png')}
            />
            <Text style={styles.title}>COMPASS</Text>
         {
          !this.state.loginClicked && !this.state.registerClicked?
            <View style={styles.buttonsContainer}>    
              <TouchableOpacity
                style={styles.loginButton}
                onPress={() => this.checkIfLoc()}
              ><View>
              <Text style={styles.buttonTextLogin}>LOGIN</Text></View></TouchableOpacity>

                        
              <TouchableOpacity
                style={styles.registerButton}
                onPress={() =>  this.setState({ registerClicked: true })}
  
              ><Text style={styles.buttonTextRegister}>SIGN UP</Text></TouchableOpacity>
            </View>
            :

            <View></View>
          }     
         

          {
            this.state.loginClicked?
            <View>
              <View style={styles.errorsContainer}><Text  style={styles.errorsText}>{ifErrors}</Text></View>
              <View style={styles.inputsContainer}>
                <TextInput
                    style={styles.input}
                    placeholder="Enter e-mail address"
                    placeholderTextColor= "#fff" 
                    onChangeText={(text) => this.setState({uidInput: text})}
                    value={this.state.uidInput}
                />
                <TextInput
                    secureTextEntry={true}
                    placeholder="Enter password"
                    placeholderTextColor= "#fff" 
                    style={styles.input}
                    onChangeText={(text) => this.setState({pwdInput: text})}
                    value={this.state.pwdInput}
                />
                <View style={styles.loginContainer}>
                  <TouchableOpacity
                    style={styles.loginButton}
                    onPress={() => this. onClickLogin()}
                    ><View>
                    <Text style={styles.buttonTextLogin}>LOGIN</Text></View></TouchableOpacity>

                  <Text  style={styles.links} onPress={() => this.setState({ loginClicked: false })}>Don’t have an account?</Text>
                </View>
              </View>
            </View>    
          :

            <View></View>
          }
         
          {
            this.state.registerClicked?
            
              this.state.registered?
              <View style={styles.successContainer}>
                <Text style={styles.errorsText}>You have registered successfully!</Text>
                <Text style={styles.errorsText}> Please check your email to verify your account before logging in</Text>
                <Text style={{ color: '#fff', textAlign:'center', marginTop: 30}} onPress={() => this.setState({ registerClicked: false, loginClicked: true })}>Login here!</Text>
              </View>
              :
              
            
              <View>
                <View style={styles.errorsContainer}><Text style={styles.errorsText}>{ifErrors}</Text></View>
                <View style={styles.inputsContainer}>
                  <TextInput
                    style={styles.input}
                    placeholder="Enter username"
                    placeholderTextColor= "#fff" 
                    onChangeText={(text) => this.setState({nameInput: text})}
                    value={this.state.nameInput}
                  />
                  <TextInput
                    style={styles.input}
                    placeholder="Enter e-mail address"
                    placeholderTextColor= "#fff" 
                    onChangeText={(text) => this.setState({emailRegister: text})}
                    value={this.state.emailRegister}
                  />
                  <TextInput
                    secureTextEntry={true}
                    placeholder="Enter password"
                    placeholderTextColor= "#fff" 
                    style={styles.input}
                    onChangeText={(text) => this.setState({pwdRegisterInput: text})}
                    value={this.state.pwdRegisterInput}
                  />
                  <TextInput
                    secureTextEntry={true}
                    placeholder="Enter password confirmation"
                    placeholderTextColor= "#fff"  
                    style={styles.input}
                    onChangeText={(text) => this.setState({pwdConfInput: text})}
                    value={this.state.pwdConfInput}
                  />
                </View>
                <View style={styles.loginContainer}>
                  <TouchableOpacity
                    style={styles.registerButton}
                    onPress={() => this. onClickRegister()}
                    ><View>
                    <Text style={styles.buttonTextRegister}>SIGN UP</Text></View></TouchableOpacity>
                    <Text  style={styles.links} onPress={() => this.setState({ registerClicked: false })}>Already have an account?</Text>
                </View>
              </View>  
            :

              <View></View>
          }    
   
        </View>
        </ScrollView>
        </ImageBackground>
        
        
      );
    }
  }
  
  const styles = StyleSheet.create({

    mainContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 50,
    },
    logo: {
      marginBottom: 5,
    },
    errorsContainer:{
        marginTop: -20,
        marginBottom: 10,
    },
    input: {
        height: 40,
        width: 200,
        backgroundColor: '#3591A1',
        marginBottom: 15, 
        borderColor: '#111', 
        borderWidth: 1,
    },
    loginButton: {
      width: 178,
      height: 48,
      marginBottom: 20,
      backgroundColor: '#fff',
      justifyContent: 'center',
      alignContent: 'center',
      borderRadius: 2,
      shadowOffset:{  width: 4,  height: 4,  },
      shadowColor: 'black',
      shadowOpacity: 0.25,

    },
    registerButton: {
      width: 178,
      height: 48,
      backgroundColor: '#1A1A1A',
      justifyContent: 'center',
      alignContent: 'center',
      borderRadius: 2,
      shadowOffset:{  width: 4,  height: 4,  },
      shadowColor: 'black',
      shadowOpacity: 0.25,
    },
    buttonsContainer:{
      width: 178,
      height: 200,
    },
    title:{
      fontSize: 48,
      marginBottom: 50,
      fontFamily: 'FjallaOne-Regular',
      color: '#fff'
    },
    backgroundImage: {
      flex: 1,
      resizeMode: 'cover', // or 'stretch'
    },
    buttonTextLogin:{
      textAlign:'center',
      fontSize: 22,
      fontFamily: 'FjallaOne-Regular',
      color: '#0E7385',
    },
    buttonTextRegister:{
      textAlign:'center',
      fontSize: 22,
      fontFamily: 'FjallaOne-Regular',
      color: '#fff',
    },
    links:{
      color: '#fff',
      marginBottom: 10,
      marginTop: 15
    }, 
    loginContainer:{
      textAlign:'center',
      justifyContent: 'center',
      alignItems: 'center',
    },
    errorsText:{
      color: '#fff',
      textAlign:'center',
    },
    inputsContainer:{
      textAlign:'center',
      justifyContent: 'center',
      alignItems: 'center',
    }

  });

  AppRegistry.registerComponent('StartingPage', () => StartingPage);