import React, {Component} from 'react';
import {BackHandler} from 'react-native';
import { withNavigation } from 'react-navigation';



class Logout extends Component{
    
    constructor(props) {
      super(props);
      this.state = {
    
      };
    }

    //calls restful API to logout
    componentDidMount = () => {
      
        const URI = 'http://ec2-18-219-91-214.us-east-2.compute.amazonaws.com/compass-restful-api/public/api/auth/logout';//local

        const { navigation } = this.props;

        var token =  navigation.getParam('token', 'NO-TOKEN');

        console.log(token);
  
        return fetch(URI,
        { 
            method: 'POST',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
            }
        }
        )
        .then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson);
          BackHandler.exitApp();
        })
        .catch((error) => {
            console.log(error);
        })
      
    }
    
    render() {
   
      return (
       null
      );
    }
  }
  


  export default withNavigation(Logout);