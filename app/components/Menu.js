import React, {Component} from 'react';
import {StyleSheet, View, AppRegistry, Image, TouchableOpacity, Text, ImageBackground} from 'react-native';
import Navbar from './Navbar';

export default class Menu extends Component{
    
    constructor(props) {
      super(props);
      this.state = {
        //here if needed
        loggedIn: null,
        token: null,
        inputLinkClicked: false,
        menu: true,
      };
    }
    
    componentDidMount() {

      const { navigation } = this.props;

      this.setState({
    
        loggedIn: navigation.getParam('loggedIn', 'NO-LOGGEDIN'),
        token: navigation.getParam('token', 'NO-TOKEN')
          
      });   
    }

   
    
    render() {
   
      return (
     
        <View style={styles.container}>
        <ImageBackground source={require('../../assets/bg.png')} style={{flex:1, height: undefined, width: undefined}} >
          <View style={styles.headerStyle}>
            <Navbar token = {this.state.token} menu = {this.state.menu}></Navbar>
          </View>
          <View style={[{flex: 1}]}>
            <View style={styles.titleContainer}>
              <Text style={styles.titleText}>WHAT ARE YOU LOOKING FOR?</Text>
            </View>

            <View style={styles.iconsContainer}>
           
                <TouchableOpacity onPress={() => this.props.navigation.navigate('FetchPlaces', {
                categoryID: 0, //sends categoryID to the next page
                loggedIn: this.state.loggedIn,
                token: this.state.token
              })}>
                <Image
                  style={styles.image}
                  source={require('../../assets/iconRestaurants.png')}
                />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('FetchPlaces', {
                categoryID: 1,
                apiCategory: '4bf58dd8d48988d116941735',
                token: this.state.token
              })}>
                <Image
                  style={styles.image}
                  source={require('../../assets/iconBars.png')}
                />
              </TouchableOpacity>  
              <TouchableOpacity onPress={() => this.props.navigation.navigate('FetchPlaces', {
                  categoryID: 2,
                  token: this.state.token
              })}>
              <Image
                style={styles.image}
                source={require('../../assets/iconEvents.png')}
              />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('FetchPlaces', {
                categoryID: 3,
                apiCategory: '4bf58dd8d48988d181941735',
                token: this.state.token
              })}>
                <Image
                  style={styles.image}
                  source={require('../../assets/iconMusea.png')}
                />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('FetchPlaces', {
                categoryID: 4,
                apiCategory: '4deefb944765f83613cdba6e',
                token: this.state.token
              })}>
                <Image
                  style={styles.image}
                  source={require('../../assets/iconSites.png')}
                />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('FetchPlaces', {
                categoryID: 5,
                apiCategory: '5744ccdfe4b0c0459246b4c4',
                token: this.state.token
              })}>
                <Image
                  style={styles.image}
                  source={require('../../assets/iconToilets.png')}
                />
              </TouchableOpacity>
              

            </View>
          </View>
        </ImageBackground>
      </View>
      );
    }
  }
  
  const styles = StyleSheet.create({

    container: {
      flex: 1,
    },
    headerStyle: {
      marginBottom: 50,

    },
    titleContainer:{
      width: '100%', 
      height: 85,
      backgroundColor: '#fff',
      justifyContent: 'center',
      alignContent: 'center',
    },
    titleText:{
      textAlign:'center',
      fontSize: 26,
      color: '#111',
      fontFamily: 'FjallaOne-Regular',
    },  
    iconsContainer: {
      flex: 2,
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'center',
      alignContent: 'center',
    },  
    placeTitle: {
      textAlign:'center',
      fontSize: 24,
      color: '#fff',
      fontFamily: 'FjallaOne-Regular',
    },
    image : {
      width: 110, 
      height: 105,
      marginBottom: 100,
      shadowOffset:{  width: 4,  height: 4,  },
      shadowColor: '#fff',
      shadowOpacity: 0.50,
    },
  
  });

  AppRegistry.registerComponent('Menu', () => Menu);