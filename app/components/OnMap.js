import React, {Component} from 'react';
import {StyleSheet, View, AppRegistry, TouchableOpacity, Text, ImageBackground} from 'react-native';
import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import {Icon, Rating} from 'react-native-elements';
import Navbar from './Navbar';

export default class OnMap extends Component{

    constructor(props) {
      super(props);
      this.state = {
        userLocation: null,
        placeLocation: null,
        directions: null,
        dataSource: null,
        placeName: null,
        token:null,
        error: null,
        address: null,
      };
    }
    
    componentDidMount() {

      const { navigation } = this.props;
      
      //gets user and place position from previous component
      navigator.geolocation.getCurrentPosition(position => {
        this.setState({
          coordinates: [],
          userLocation:{
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: 0.00822,
            longitudeDelta: 0.00821,
          },
          placeLocation:{
            latitude: parseFloat(navigation.getParam('lat', 'NO-LATITUDE')),
            longitude: parseFloat(navigation.getParam('lng', 'NO-LONGITUDE')),
          },
          address: navigation.getParam('address', 'NO-PLACENAME'),
          placeName: navigation.getParam('placeName', 'NO-PLACENAME'),
          rating: navigation.getParam('rating', 'NO-RATING'),
          token: navigation.getParam('token', 'NO-LONGITUDE')
        })
      });
     
    }

    //gets directions
    onClickDirections = (mode, color) => {

      const key = "AIzaSyAm7GbaRHzHLW-ef6sizCUNc6Ed4iN2Bpw";

      this.setState({

        directions: null, 
         
      });

      return fetch('https://maps.googleapis.com/maps/api/directions/json?origin='+this.state.userLocation.latitude +','+ this.state.userLocation.longitude+'&destination='+this.state.placeLocation.latitude + ','+ this.state.placeLocation.longitude+'&mode='+mode+'&key=AIzaSyAm7GbaRHzHLW-ef6sizCUNc6Ed4iN2Bpw')
      .then((response) => response.json())
      .then((responseJson) => {

        this.setState({
          dataSource: responseJson.routes[0].legs[0],
          directions:  <MapViewDirections
            origin={this.state.userLocation}
            destination={this.state.placeLocation}
            mode= {mode}
            apikey={key}
            strokeWidth={2}
            strokeColor= {color}
            optimizeWaypoints= {true} 
          />

        });
      })
      .catch((error) => {

        this.setState({
    
          error: error //updates error state

        });

      });
    }

   
  
    render() {

      var userLocationMarker = null;
      var placeLocationMarker = null;

      if(this.state.userLocation){

        userLocationMarker = <MapView.Marker coordinate={this.state.userLocation} />;
        placeLocationMarker = <MapView.Marker coordinate={this.state.placeLocation} />;

      }
      
      return (
       
      <View style={styles.container}>
        <ImageBackground source={require('../../assets/bg.png')} style={{flex:1, height: undefined, width: undefined}} >
          <View style={styles.headerStyle}>
            <Navbar token = {this.state.token}></Navbar>
          </View>
          <View style={[{flex: 1}]}>
            <View style={styles.placeInfoContainer}>
              <Text style={styles.placeTitle}>{this.state.placeName}</Text>
              <Text style={styles.address}>{this.state.address}</Text>
              {
                this.state.rating === "NO-RATING"?
                null
                :
                <Rating
                  type='custom'
                  ratingBackgroundColor='#c8c7c8'
                  imageSize={20}
                  readonly
                  startingValue={parseFloat(this.state.rating)}
                  />
              }
            </View>
            <MapView style={styles.map} region={this.state.userLocation}>
              {userLocationMarker}
              {placeLocationMarker}
              {this.state.directions}
            </MapView>
            
            <View style={{flex: 2}}>
            
              <View style={styles.optionsContainer}>
                <TouchableOpacity onPress={() => this.onClickDirections("walking", "red")} style={styles.optionButton}>
                  <Icon name='directions-walk' color='#fff' size={28}/>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.onClickDirections("bicycling", "blue")} style={styles.optionButton}>  
                  <View>
                    <Icon name='directions-car' color='#fff' size={28}/>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.onClickDirections("driving", "green")} style={styles.optionButton}>  
                  <View>
                    <Icon name='directions-bike' color='#fff' size={28}/>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.infoContainer}>
                  {this.state.dataSource && <Text style={styles.infoText}>Distance: {this.state.dataSource.distance.text}  </Text>}
                  {this.state.dataSource && <Text style={styles.infoText}>Duration: {this.state.dataSource.duration.text}  </Text>}
              </View>
            </View>
          </View>
        </ImageBackground>
      </View>
      );
    }
  }
  
  const styles = StyleSheet.create({

    container: {
      flex: 1
    },
    headerStyle: {
      marginBottom: 50
    },
    placeInfoContainer: {
      width: '100%',
      height: 90,
      backgroundColor: '#100A2A',
      justifyContent: 'center',
      alignContent: 'center',
    },
    placeTitle: {
      textAlign:'center',
      fontSize: 24,
      color: '#fff',
      fontFamily: 'FjallaOne-Regular',
    },
    map:{
      width: '100%',
      height: '50%'
    },
    optionsContainer: {
      flexDirection: 'row',
      justifyContent: 'space-around',   
    },
    optionButton: {
      width: '33%',
      height: 45,
      backgroundColor: '#111',
      justifyContent: 'center',
      alignContent: 'center',
    },
    infoContainer: {
      width: '100%',
      height: '50%',
      justifyContent: 'center',
      alignContent: 'center',
      marginTop: 30,
    },
    infoText: {
      textAlign:'center',
      fontSize: 18,
      fontFamily: 'FjallaOne-Regular',
      color: '#fff',
    },
    address: {
      textAlign:'center',
      color: '#fff',
      fontFamily: 'FjallaOne-Regular',
      marginBottom: 10
    }
   
  });

  AppRegistry.registerComponent('OnMap', () => OnMap);

